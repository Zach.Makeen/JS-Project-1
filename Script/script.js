'use strict';

document.addEventListener('DOMContentLoaded', setup);

//Global variables
let todoArray = [];
let form;
let category;
let taskNumber = 0;
let tasks;
let id;

/*
 *@desc When the DOM is loaded, the setup will load existing tasks in the local storage
 *      if there are any and create an event for when the submit button is clicked.
 *@param none
 */
function setup() {    
    loadTodo();
    form = document.querySelector('form');
    form.addEventListener('submit', submit);
}

/*
 *@desc The Todo class creates a new object containing all of the forms inputs.
 *@param task, description, importance, category
 */
class Todo {
    constructor(task, description, importance, category) {
        this.id = taskNumber;
        this.task = task;
        this.description = description;
        this.importance = importance;
        this.category = category;
    }
}

/*
 *@desc The submit function gets the values the user entered and stores them into the
 *      global array using objects. It then stringifies these values and stores it into the
 *      local storage.
 *@param e
 */
function submit(e) {
    if(form.checkValidity()) {
        let task = document.querySelector('#task').value;
        let description = document.querySelector('#description').value;
        let importance = document.querySelector('#importance').value;
        let radioSet = document.getElementsByName('category');
        for(let i in radioSet) {
            if(radioSet[i].checked) {
                category = radioSet[i].value;
            }
        }
        todoArray.push(new Todo(task, description, importance, category));
        //
        console.log(todoArray);
        //
        localStorage.setItem('todo', JSON.stringify(todoArray));
        printTask(task, description, importance, category);
        form.reset();
        document.getElementById("amount").innerHTML = 1;
        e.preventDefault();
    }
}

/*
 *@desc The loadTodo function is called when the DOM is loaded and prints all the arrays
 *      within the local storage.
 */
function loadTodo() {
    if(localStorage.getItem('todo')) {
        todoArray = JSON.parse(localStorage.getItem('todo'));
        for(let i = 0; i < todoArray.length; i++) {
            printTask(todoArray[i].task, todoArray[i].description, todoArray[i].importance, todoArray[i].category);
        }
    }
}

/*
 *@desc The printTask function creates the HTML to create the new task and appends it
 *      to the <section> tag we created in the index.html. It then creates and event for when
 *      the checkbox is checked.
 *@param task, description, importance, category
 */
function printTask(task, description, importance, category) {
    let section = document.querySelector('.tasks');
    tasks = document.createElement('div');
    tasks.innerHTML =   `<div id = ${taskNumber}>
                            <div class="row">
                                <div class="column">
                                    <p class = "task">${task}</p>
                                    <p class = "description">${description}</p>
                                </div>
                                <div class="column">
                                    <p class = "importance">Importance: ${importance}</p>
                                    <p class = "category">For: ${category}</p>
                                </div>
                                <div class="button">
                                    <input type="checkbox" class = "removeTask${taskNumber}" value = "finished">
                                </div>
                            </div>
                        </div>`;
    section.appendChild(tasks);
    let checkbox = document.querySelector(`.removeTask${taskNumber}`);
    checkbox.addEventListener('click', checkedAction);
    taskNumber++;
}

/*
 *@desc The checkedAction function removes the HTML of the task from the DOM,
 *      the global array, and the local storage.
 *@param e
 */
function checkedAction(e) {
    e.currentTarget.parentElement.parentElement.parentElement.remove();
    let taskForm = e.currentTarget.parentElement.parentElement.parentElement.getAttribute('id');
    for(let i = 0; i < todoArray.length;i++) {
        if(taskForm == todoArray[i].id) {
            todoArray.splice(i, 1);
            break;
        }
    }
    localStorage.setItem('todo', JSON.stringify(todoArray));
    taskNumber--;
}

//This changes the value displayed for the range bar.
document.getElementById("importance").oninput = function() {
    let value = document.getElementById("importance").value;
    document.getElementById("amount").innerHTML = value;
}